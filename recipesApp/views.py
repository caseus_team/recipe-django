from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from recipesApp.models import Category, Ingredient, Recipe
from recipesApp.serializers import CategorySerializer, IngredientSerializer, RecipeGetSerializer, \
    RecipeCreateSerializer, RecipeUpdateSerializer


# get
class CategoryList(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


# get
class CategoryRetrieve(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    lookup_field = 'id'


# get
class IngredientList(generics.ListAPIView):
    queryset = Ingredient.objects.all()
    serializer_class = IngredientSerializer


# post
class IngredientCreate(generics.CreateAPIView):
    serializer_class = IngredientSerializer


# get
class RecipeListOfCategory(generics.ListAPIView):
    serializer_class = RecipeGetSerializer

    def get_queryset(self):
        category_id = self.kwargs['categoryId']
        return Recipe.objects.filter(category__id=category_id)


# get, delete
class RecipeRetrieveDelete(generics.RetrieveDestroyAPIView):
    queryset = Recipe.objects.all()
    lookup_field = 'id'
    serializer_class = RecipeGetSerializer


# put, patch
class RecipeSave(generics.UpdateAPIView):
    queryset = Recipe.objects.all()
    lookup_field = 'id'
    serializer_class = RecipeUpdateSerializer


# post
class RecipeCreate(generics.CreateAPIView):
    serializer_class = RecipeCreateSerializer


# post
@api_view(['POST'])
def recipe_list_with_ingredients(request):
    request_ingredients = list(map(lambda ingredient: ingredient['id'], request.data))
    recipes = []
    for recipe in Recipe.objects.all().iterator():
        recipe_ingredients = list(map(lambda ingredient: ingredient.ingredient.id, list(recipe.ingredients.all())))
        if all(elem in recipe_ingredients for elem in request_ingredients):
            recipes.append(RecipeGetSerializer(recipe).data)
    return Response(recipes)
