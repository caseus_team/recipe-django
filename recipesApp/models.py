from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=1000, blank=True, null=True)
    description = models.CharField(max_length=5000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'category'


class Ingredient(models.Model):
    name = models.CharField(max_length=1000, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ingredient'


class Recipe(models.Model):
    name = models.CharField(max_length=1000, blank=True, null=True)
    brief_description = models.CharField(max_length=2000, blank=True, null=True)
    description = models.CharField(max_length=5000, blank=True, null=True)
    category = models.ForeignKey(Category, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'recipe'


class RecipeIngredient(models.Model):
    amount = models.CharField(max_length=200, blank=True, null=True)
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='ingredients', null=True, blank=True)
    ingredient = models.ForeignKey(Ingredient, on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'recipe_ingredient'
