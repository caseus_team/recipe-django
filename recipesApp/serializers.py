from rest_framework import serializers
from recipesApp.models import Category, Ingredient, Recipe, RecipeIngredient


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        extra_kwargs = {'id': {'read_only': False}}


class IngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ingredient
        fields = ('id', 'name')


class RecipeIngredientGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeIngredient
        fields = ('id', 'amount', 'ingredient')
        depth = 1


class RecipeIngredientCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeIngredient
        fields = ('id', 'amount', 'ingredient')


class RecipeIngredientListSerializer(serializers.ListSerializer):

    def update(self, instance, validated_data):
        recipe_ingredient_mapping = {recipe_ingredient.id: recipe_ingredient for recipe_ingredient in instance}
        data_mapping = {recipe_ingredient['id']: recipe_ingredient for recipe_ingredient in validated_data}

        ret = []
        for recipe_ingredient_id, data in data_mapping.items():
            recipe_ingredient = recipe_ingredient_mapping.get(recipe_ingredient_id, None)
            if recipe_ingredient is None:
                data['id'] = None
                ret.append(self.child.create(data))
            else:
                ret.append(self.child.update(recipe_ingredient, data))

        for recipe_ingredient_id, recipe_ingredient in recipe_ingredient_mapping.items():
            if recipe_ingredient_id not in data_mapping:
                recipe_ingredient.delete()

        return ret


class RecipeIngredientUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = RecipeIngredient
        fields = ('id', 'amount', 'ingredient')
        extra_kwargs = {'id': {'read_only': False}}
        list_serializer_class = RecipeIngredientListSerializer


class RecipeCreateSerializer(serializers.ModelSerializer):
    ingredients = RecipeIngredientCreateSerializer(many=True)
    briefDescription = serializers.CharField(source='brief_description')

    def create(self, validated_data):
        ingredients = validated_data.pop('ingredients')
        recipe = Recipe.objects.create(**validated_data)
        for ingredient in ingredients:
            RecipeIngredient.objects.create(recipe=recipe, **ingredient)
        return recipe

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'description', 'category', 'ingredients', 'briefDescription')


class RecipeUpdateSerializer(serializers.ModelSerializer):
    ingredients = RecipeIngredientUpdateSerializer(many=True)
    briefDescription = serializers.CharField(source='brief_description')

    def update(self, instance, validated_data):
        nested_serializer = self.fields['ingredients']
        ingredients_data = validated_data.pop('ingredients')
        ingredients = list(instance.ingredients.all())
        updated = nested_serializer.update(ingredients, ingredients_data)
        for ingredient in updated:
            ingredient.recipe = instance
            ingredient.save()
        instance.name = validated_data.get('name', instance.name)
        instance.brief_description = validated_data.get('brief_description', instance.brief_description)
        instance.description = validated_data.get('description', instance.description)
        instance.category = validated_data.get('category', instance.category)
        instance.save()
        return instance

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'description', 'category', 'ingredients', 'briefDescription')


class RecipeGetSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    ingredients = RecipeIngredientGetSerializer(many=True)
    briefDescription = serializers.CharField(source='brief_description')

    class Meta:
        model = Recipe
        fields = ('id', 'name', 'description', 'category', 'ingredients', 'briefDescription')
