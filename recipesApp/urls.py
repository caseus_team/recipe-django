from django.urls import path

from . import views

urlpatterns = [
    path('category/fetchAll/', views.CategoryList.as_view()),
    path('category/fetch/<int:id>/', views.CategoryRetrieve.as_view()),
    path('ingredient/fetchAll/', views.IngredientList.as_view()),
    path('ingredient/save/', views.IngredientCreate.as_view()),
    path('recipe/fetchAll/<int:categoryId>/', views.RecipeListOfCategory.as_view()),
    path('recipe/fetch/<int:id>/', views.RecipeRetrieveDelete.as_view()),
    path('recipe/delete/<int:id>/', views.RecipeRetrieveDelete.as_view()),
    path('recipe/save/<int:id>/', views.RecipeSave.as_view()),
    path('recipe/save/', views.RecipeCreate.as_view()),
    path('recipe/fetchAll/', views.recipe_list_with_ingredients),
]
